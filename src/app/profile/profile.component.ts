import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser:any;
  profilecontent?: String;
  message: any;
  currentIndex = -1;


  constructor(private token: TokenStorageService,
    private userService: UserService
    ) { }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.message = '';

  }



  refreshList(): void {
    this.currentUser = undefined;
    this.currentIndex = -1;
  }

  updateProfile(): void {
    
      this.userService.putupdateProfile(this.currentUser.id, this.currentUser)
        .subscribe(
          response => {
            console.log(response);
            this.message = response.message;
            console.log(response);
            //this.profilecontent = response.profilecontent;

         },
          error => {
            console.log(error);
         });

    }
    

}
