export class Crud {
    id?: any;
    title?: string;
    username?: string;
    description?: string;
    published?: boolean;
    createdAt?: any;
    updatedAt?: any;
  }