import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Crud } from 'src/app/models/crud.model';
import { CrudService } from 'src/app/services/crud.service';
import { TokenStorageService } from '../services/token-storage.service';


@Component({
  selector: 'app-board-user',
  templateUrl: './board-user.component.html',
  styleUrls: ['./board-user.component.css']
})
export class BoardUserComponent implements OnInit {
  content?: {
    message: String;
    status: String;
  }
  cruds?: Crud[];
  currentCrud?: Crud;
  currentIndex = -1;
  title = '';
  currentUser:any;

 
  get loginSuccess() {
    return this.content?.status == "success";
  };
  get loginFail() {
    return !this.loginSuccess;
  }

  constructor(private userService: UserService,
    private crudService: CrudService,
    private token: TokenStorageService
    ) { }

  ngOnInit(): void {
    this.retrieveCruds();

    this.userService.getUserBoard().subscribe(
      data => {
        this.content = JSON.parse(data);
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
    this.currentUser = this.token.getUser();
  }



  retrieveCruds(): void {
    this.crudService.getAll()
      .subscribe(
        data => {
          this.cruds = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveCruds();
    this.currentCrud = undefined;
    this.currentIndex = -1;
  }

  setActiveCrud(crud: Crud, index: number): void {
    this.currentCrud = crud;
    this.currentIndex = index;
  }

  removeAllCruds(): void {
    this.crudService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle(): void {
    this.crudService.findByTitle(this.title)
      .subscribe(
        data => {
          this.cruds = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
