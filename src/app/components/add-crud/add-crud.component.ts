import { Component, OnInit } from '@angular/core';
import { Crud } from 'src/app/models/crud.model';
import { CrudService } from 'src/app/services/crud.service';
import { UserService } from '../../services/user.service';
import { TokenStorageService } from '../../services/token-storage.service';


@Component({
  selector: 'app-add-crud',
  templateUrl: './add-crud.component.html',
  styleUrls: ['./add-crud.component.css']
})
export class AddCrudComponent implements OnInit {
  content?: {
    message: String;
    status: String;
  }
  crud: Crud = {
    title: '',
    description: '',
    username: '',
    published: false
  };
  currentUser:any;




  submitted = false;

  get loginSuccess() {
    return this.content?.status == "success";
  };
  get loginFail() {
    return !this.loginSuccess;
  }

  constructor(private crudService: CrudService,
    private userService: UserService,
    private token: TokenStorageService) { }

  ngOnInit(): void {
    this.userService.getUserBoard().subscribe(
      data => {
        this.content = JSON.parse(data);
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
    this.currentUser = this.token.getUser();
  }

  saveCrud(): void {
    const data = {
      title: this.crud.title,
      description: this.crud.description,
      username: this.currentUser.username
      

    };

    this.crudService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newCrud(): void {
    this.submitted = false;
    this.crud = {
      title: '',
      description: '',
      published: false
    };
  }

}