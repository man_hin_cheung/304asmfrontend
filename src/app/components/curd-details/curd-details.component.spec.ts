import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurdDetailsComponent } from './curd-details.component';

describe('CurdDetailsComponent', () => {
  let component: CurdDetailsComponent;
  let fixture: ComponentFixture<CurdDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurdDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurdDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
