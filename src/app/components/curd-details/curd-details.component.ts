import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Crud } from 'src/app/models/crud.model';


@Component({
  selector: 'app-curd-details',
  templateUrl: './curd-details.component.html',
  styleUrls: ['./curd-details.component.css']
})
export class CurdDetailsComponent implements OnInit {
  currentCrud: Crud = {
    title: '',
    description: '',
    published: false
  };
  message = '';
  constructor(
    private crudService: CrudService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getCrud(this.route.snapshot.params.id);
  }

  getCrud(id: string): void {
    this.crudService.get(id)
      .subscribe(
        data => {
          this.currentCrud = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updatePublished(status: boolean): void {
    const data = {
      title: this.currentCrud.title,
      description: this.currentCrud.description,
      published: status
    };

    this.crudService.update(this.currentCrud.id, data)
      .subscribe(
        response => {
          this.currentCrud.published = status;
          console.log(response);
          this.message = response.message;
        },
        error => {
          console.log(error);
        });
  }

  updateCrud(): void {
    this.crudService.update(this.currentCrud.id, this.currentCrud)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message;
        },
        error => {
          console.log(error);
        });
  }

  deleteCrud(): void {
    this.crudService.delete(this.currentCrud.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/user/cruds']);
        },
        error => {
          console.log(error);
        });
  }
}