import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';

import { CurdDetailsComponent } from './components/curd-details/curd-details.component';
import { AddCrudComponent } from './components/add-crud/add-crud.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  //{ path: 'user', component: BoardUserComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },

  //{ path: '', redirectTo: 'user/cruds', pathMatch: 'full' },
  //{ path: 'user/cruds', component: BoardUserComponent },
  { path: 'user/cruds', component: BoardUserComponent },
  { path: 'user/cruds/:id', component: CurdDetailsComponent },
  { path: 'user/add', component: AddCrudComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
